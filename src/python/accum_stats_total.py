import argparse
from os import path

from stats import prepare_data_for_plotting, plot_metric

def plot(file, output_file):
    gg = prepare_data_for_plotting(file, lambda x:x)
    gg = plot_metric(gg, "total", "ms", "")
    gg.save(output_file, width=9, height=5, bg="transparent")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to generate graphs for an Ohua execution trace.')
    parser.add_argument('-f', nargs=1, help='Reference to the trace file (json).')
    parser.add_argument('-o', nargs=1, help='Name/path of output file.')

    args = parser.parse_args()
    plot(args.f[0], args.o[0])
