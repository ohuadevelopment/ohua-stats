import pandas as pd
import json

import rpy2.robjects.lib.ggplot2 as ggplot2
import rpy2.robjects
import rpy2.robjects.lib.grid as grid
# import rpy2.rinterface as ri

def get_data(file):
    return json.load(open(file), strict=False)

def prepare_all_data(data):
    for d in data:
        e = {}
        for k,v in d['stats'].iteritems():
            if len(v) > 0: e[k] = v
        d['stats'] = e
    return data

def prepare_frame(data):
    func_ids = []
    frames = []
    for d in data:
        func_ids.append(d['func-id'])
        frames.append(pd.DataFrame.from_dict(d['stats'], orient='index'))

    d = pd.concat(frames, keys=func_ids)
    d.reset_index(inplace=True)
    # print d
    # print d.columns
    new_columns = d.columns.values
    new_columns[0] = "funcid"
    new_columns[1] = "stat"
    d.columns = new_columns
    # print d
    return d

total_exec_title = None

def set_total_exec_title(val):
    global total_exec_title
    total_exec_title = val

def plot_metric(gg,  metric, unit, title):
    return gg + \
         ggplot2.aes_string(x='funcid', y=metric, fill='stat') + \
         ggplot2.geom_bar(position="dodge", stat="identity") + \
         ggplot2.labs(x="", y='exec time [' + unit + ']') + \
         ggplot2.ggtitle(title) + \
         ggplot2.theme(**{'axis.text.x' : ggplot2.element_text(angle=45, hjust=1, vjust=0.98),
                          'plot.margin' : grid.unit(rpy2.robjects.vectors.FloatVector([0.5, 1, 0.5, 5]), "lines")})

def prepare_data_for_plotting(file, prepare_data):
    melted = prepare_frame(prepare_all_data(prepare_data(get_data(file))))

    from rpy2.robjects import pandas2ri
    pandas2ri.activate()
    r_melted = pandas2ri.py2ri(melted)
    #
    # print r_melted

    # ri.initr()
    # ... code ...
    # ri.endr()

    gg = ggplot2.ggplot(r_melted)
    return gg

def plot(file, prepare_data, output_file, additional=[]):
    gg = prepare_data_for_plotting(file, prepare_data)

    from rpy2.robjects.packages import importr
    grdevices = importr('grDevices')

    grdevices.pdf(file=output_file+".pdf", width=10, height=20)
    # plotting code here

    grid.newpage()
    vp = grid.viewport(layout=grid.layout(3 + len(additional), 1))
    vp.push()

    vp = grid.viewport(**{'layout.pos.col':1, 'layout.pos.row': 1})
    pp = gg + \
         ggplot2.aes_string(x='funcid', y='avg', fill='stat') + \
         ggplot2.geom_bar(position="dodge", stat="identity") + \
         ggplot2.geom_errorbar(ggplot2.aes_string(ymax='max', ymin='min'), position=ggplot2.position_dodge(width=0.9), width=0.25) + \
         ggplot2.labs(x="", y="mean exec time [ns]") + \
         ggplot2.ggtitle('Mean exec time with min/max') + \
         ggplot2.theme(**{'axis.text.x' : ggplot2.element_text(angle=45, hjust=1, vjust=0.98),
                          'plot.margin' : grid.unit(rpy2.robjects.vectors.FloatVector([1, 1, 0.5, 5]), "lines")})
    pp.plot(vp=vp)

    metrics = [{'metric' : 'avg', 'title' : 'Mean exec time', 'unit' : 'ns'}]
    global total_exec_title
    if total_exec_title:
        metrics.append({'metric' : 'total', 'title' : 'Total exec time\n' + total_exec_title , 'unit' : 'ms'})
    else:
        metrics.append({'metric' : 'total', 'title' : 'Total exec time', 'unit' : 'ms'})

    metrics.extend(additional)
    i = 2
    for m in metrics:
        vp = grid.viewport(**{'layout.pos.col':1, 'layout.pos.row': i})
        pp = plot_metric(gg, m['metric'], m['unit'], m['title'])
        pp.plot(vp=vp)
        i += 1

    grdevices.dev_off()
