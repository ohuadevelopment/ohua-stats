import numpy

import sys
import re

#[GC pause (G1 Evacuation Pause) (young), 1.7789007 secs]
# pattern = re.compile("\[GC pause \(G1 Evacuation Pause\) \(young\), ([0-9\.]*) secs\]")
pattern = re.compile("\[GC pause [\(\)A-Za-z0-9\s]*, ([0-9\.]*) secs\]")

def read_gc_data(file, start_pattern):
    start_regex = re.compile("^" + str(start_pattern))
    perf_data = []
    collect = False
    # read the gc lines from the time where the execution started
    for line in open(file, 'r'):
        if collect:
            match = pattern.match(line)
            if match:
                perf_data.append(float(match.group(1)))
        else:
            collect = start_regex.match(line)
    return perf_data

if __name__ == '__main__':
    d = read_gc_data(sys.argv[1], sys.argv[2])
    total = numpy.sum(d)
    print "Total pause time: " + str(total) + " secs"
