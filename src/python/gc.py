import numpy

import sys
import re

#Example: [GC (Allocation Failure)  42181K->8105K(126720K), 0.0068224 secs]
#[GC (Metadata GC Threshold)  645753K->19118K(1732608K), 0.0234550 secs]
#[Full GC (Metadata GC Threshold)  19118K->17935K(1934848K), 0.1127283 secs]
#[GC (GCLocker Initiated GC)  595283K->846618K(2714112K), 0.1753938 secs]
#[Full GC (Ergonomics)  846618K->500115K(4033536K), 0.2342767 secs]

#[GC (Allocation Failure)  3770412K->1134017K(4965888K), 0.0582221 secs]
pattern = re.compile("\[[0-9a-zA-Z\(\)\s\-\>]*, ([0-9\.]*) secs\]")

def read_gc_data(file):
    perf_data = []
    # read the gc lines from the time where the execution started
    for line in open(file, 'r'):
        match = pattern.match(line)
        if match:
            perf_data.append(float(match.group(1)))
    return perf_data

if __name__ == '__main__':

    d = read_gc_data(sys.argv[1])
    total = numpy.sum(d)
    print total