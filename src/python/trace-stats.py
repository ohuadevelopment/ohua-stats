import argparse
from os import path

import numpy

from stats import plot


def prepare_data(data):
    for func_data in data:
        stats = func_data["stats"]
        for key, value in stats.iteritems():
            trace = None
            if not value:
                trace = [[0, 0]]
            else:
                trace = value
            stats[key] = {
                "avg": numpy.mean(map(lambda a: a[1] - a[0], trace)),
                "min": reduce(lambda a, b: min(a, b[1] - b[0]), trace, 5555555555),
                "max": reduce(lambda a, b: max(a, b[1] - b[0]), trace, 0),
                "total": numpy.divide(reduce(lambda a, b: a + (b[1] - b[0]), trace, 0), 1000.0),
                "fifthpercentile": numpy.percentile(map(lambda a: a[1] - a[0], trace), 5),
                "nintyfifthpercentile": numpy.percentile(map(lambda a: a[1] - a[0], trace), 95)
            }
    # print data
    return data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to generate graphs for an Ohua execution trace.')
    parser.add_argument('-f', nargs=1, help='Reference to the trace file (json).')

    args = parser.parse_args()
    plot(args.f[0], prepare_data,
         "trace-stats-overview-" + path.basename(args.f[0]).split(".")[0],
         [{'metric': 'fifthpercentile',
           'unit': 'ns',
           'title': '5th percentile'},
          {'metric': 'nintyfifthpercentile',
           'unit': 'ns',
           'title': '95th percentile'}])
