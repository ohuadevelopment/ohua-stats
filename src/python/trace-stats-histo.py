import argparse
from os import path
import numpy
from stats import get_data
import pandas as pd
import rpy2.robjects.lib.ggplot2 as ggplot2


def prepare_data(data):
    f = {'funcid' : [],
         'calltime' : []}
    for func_data in data:
        stats = func_data["stats"]
        f['funcid'].extend(map(lambda a: func_data['func-id'], stats['framework']))
        f['calltime'].extend(map(lambda a: a[1] - a[0], stats['framework']))
    # print f
    return f


def prepare_frame(data):
    return pd.DataFrame.from_dict(data)


def plot(frame, out_file_name):
    # print frame
    from rpy2.robjects import pandas2ri
    pandas2ri.activate()
    r_melted = pandas2ri.py2ri(frame)
    # print r_melted

    gg = ggplot2.ggplot(r_melted) + \
         ggplot2.aes_string(x='calltime') + \
         ggplot2.labs(x="framework exec time [ns]") + \
         ggplot2.geom_histogram() + \
         ggplot2.scale_x_log10() + \
         ggplot2.scale_y_log10() + \
         ggplot2.facet_wrap('funcid')#, scales="free")

    gg.save(out_file_name + ".pdf", width=15, height=10, bg="transparent")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to generate graphs for an Ohua execution trace.')
    parser.add_argument('-f', nargs=1, help='Reference to the trace file (json).')

    args = parser.parse_args()
    plot(prepare_frame(prepare_data(get_data(args.f[0]))), "histo-functions-" + path.basename(args.f[0]).split(".")[0])