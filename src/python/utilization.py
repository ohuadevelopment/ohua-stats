import argparse

import numpy

import pandas as pd

import rpy2.robjects.lib.ggplot2 as ggplot2
from rpy2.robjects.packages import importr
import rpy2.robjects
import rpy2.rinterface as ri
import rpy2.rlike.container as rpc

from stats import get_data

def concat_dicts(a, b):
    # print "function id: " + b['funcid'][0]
    # print "num calls: " + str(len(b['funcid']))
    a['funcid'].extend(b['funcid'])
    a['start'].extend(b['start'])
    a['end'].extend(b['end'])
    return a

def prepare_utilization(a):
    result = {'funcid': [] , 'exectime' : [], 'utilization' : []}
    # print "function id: " + a['funcid'][0]
    # print "num calls: " + str(len(a['funcid']))
    for funcid, start, end  in zip(a['funcid'], a['start'], a['end']):
        # add a 0
        result['funcid'].append(funcid)
        result['exectime'].append((start - 0.1))
        result['utilization'].append(0)

        # add a 1
        result['funcid'].append(funcid)
        result['exectime'].append(start)
        result['utilization'].append(1)

        # add a 1
        result['funcid'].append(funcid)
        result['exectime'].append(end)
        result['utilization'].append(1)

        # add a 0
        result['funcid'].append(funcid)
        result['exectime'].append((end + 0.1))
        result['utilization'].append(0)
    return result


def format_func_name(s):
    f = s.split("-")
    return reduce(lambda a,b: a+"-"+b, f[0:-1]) + "\n" + f[-1]

def format_funcid(s):
    d = s.split("/")
    if len(d) > 1:
        return reduce(lambda a,b: a+"/"+b, d[0:-1]) + "\n" + format_func_name(d[-1])
    else:
        return format_func_name(s)


def prepare_data(data):
    reduced = reduce(concat_dicts,
                        [
                         {'funcid': map(lambda a: format_funcid(d['func-id']), d['stats']['framework']),
                          'start': map(lambda a: a[0], d['stats']['framework']),
                          'end': map(lambda a: a[1], d['stats']['framework'])}
                         for d in data
                         ])
    # it is a shame that pandas can not go from this to a MultiIndex -> this is fixed in the next major release for pandas: 0.18.0 (August 2017) https://github.com/pandas-dev/pandas/pull/9529
    # return [
    #     {'func-id': d['func-id'],
    #      'start': map(lambda a: a[0], d['stats']['framework']),
    #      'end': map(lambda a: a[1], d['stats']['framework'])}
    #     for d in data
    #     ]

    # now normalize
    start_time = numpy.amin(reduced['start'])
    reduced['start'] = map(lambda a: a - start_time, reduced['start'])
    reduced['end'] = map(lambda a: a - start_time, reduced['end'])
    # print reduced
    return reduced


def prepare_frame(data):
    # category conversion does not work properly from pandas to rpy2
    # print data
    # the conversion needs string not unicde objects
    # data['funcid'] = map(str, data['funcid'])
    # def trace(a):
    #     print type(a)
    #     return a
    # map(trace, data['funcid'])
    # print type(data['funcid'][0])
    d = pd.DataFrame(data)
    # print d.dtypes
    # ordered_funcs = sorted(set(data['funcid']), key=lambda a:int(a.split("\n")[-1]))
    # ordered_funcs = map(str, ordered_funcs)
    # print ordered_funcs
    # d['funcid'] = d['funcid'].astype('category', categories=ordered_funcs#, ordered=True
    # )
    # print d
    # print d.dtypes
    return d


def plot(file):
    raw_data = get_data(file)
    # print raw_data
    prep_data = prepare_data(raw_data)
    # print prep_data
    util_data = prepare_utilization(prep_data)
    # print util_data
    data = prepare_frame(util_data)
    # print data
    from rpy2.robjects import pandas2ri
    pandas2ri.activate()
    r_data = pandas2ri.py2ri(data)

    ordered_funcs = sorted(set(data['funcid']), key=lambda a:int(a.split("\n")[-1]))
    r_data[1] = rpy2.robjects.vectors.FactorVector(map(lambda a: r_data[1].levels[a-1] ,r_data[1]),
                                             levels=rpy2.robjects.vectors.StrVector(ordered_funcs),
                                             ordered=True)
    # print r_data

    gg = ggplot2.ggplot(r_data) + \
         ggplot2.aes_string(x='exectime', y='utilization') + \
         ggplot2.geom_line(size=0.7) + \
         ggplot2.labs(x="execution time [ms]", y="") + \
         ggplot2.theme(**{'axis.text.y' : ggplot2.element_blank(),
                          'axis.ticks.y' : ggplot2.element_blank(),
                          'strip.text.y' : ggplot2.element_text(angle=180),
                          'panel.grid.minor.y' : ggplot2.element_blank(),
                          'panel.grid.major.y' : ggplot2.element_blank()
                          }) + \
         ggplot2.facet_wrap("funcid", switch="y", dir="v", ncol=1)

        #  ggplot2.scale_y_discrete(limits=rpy2.robjects.vectors.StrVector([0, 1, 1])) + \
    h = len(raw_data) * 2/3
    gg.save("resource_alloc.pdf", width=15, height=h, bg="transparent", limitsize=False)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to generate graphs for an Ohua execution trace.')
    parser.add_argument('-f', nargs=1, help='Reference to the trace file (json).')

    args = parser.parse_args()
    plot(args.f[0])
