import argparse
from os import path

from stats import plot, set_total_exec_title

def calculate_metrics(data, metric):
    r = map(lambda a : a['stats'][metric]['total'], data)
    s = sum(r)

    r = map(lambda a : a['stats'][metric]['total'], filter(lambda a: a['func-id'].startswith("com.ohua.lang") and not a['func-id'].startswith("com.ohua.lang/smap-io-fun"), data))
    t = sum(r)

    r = map(lambda a : a['stats'][metric]['total'], filter(lambda a: not a['func-id'].startswith("com.ohua.lang") or a['func-id'].startswith("com.ohua.lang/smap-io-fun"), data))
    u = sum(r)

    return (s, t, u)

def calculate_overheads(data):
    framework = map(lambda a : a['stats']['framework']['total'], filter(lambda a: not a['func-id'].startswith("com.ohua.lang") or a['func-id'].startswith("com.ohua.lang/smap-io-fun"), data))
    function = map(lambda a : a['stats']['function']['total'], filter(lambda a: not a['func-id'].startswith("com.ohua.lang") or a['func-id'].startswith("com.ohua.lang/smap-io-fun"), data))
    non_algo_fns = sum(map(lambda x : x[0]-x[1], zip(framework, function)))

    algo_fns = sum(map(lambda a : a['stats']['framework']['total'], filter(lambda a: a['func-id'].startswith("com.ohua.lang") and not a['func-id'].startswith("com.ohua.lang/smap-io-fun"), data)))

    return (non_algo_fns, algo_fns)

def prepare_data(data):
    # print data
    (s,t,u) = calculate_metrics(data, "framework")
    frame_metric = "Framework: total=" + str(s) + "ms , ohua=" + str(t) + "ms , other=" + str(u) + "ms"

    if data[0]['stats']['function']:
        (a,b,c) = calculate_metrics(data, "function")
        fun_metric = "Function: total=" + str(a) + "ms , ohua=" + str(b) + "ms , other=" + str(c) + "ms"
        (o, p) = calculate_overheads(data)
        overheads = "Overheads:\n operator (sfn execution): " + str(o) + " ms\n language (algo ops): " + str(p) + " ms\n total: " + str(o+p) + " ms"
        set_total_exec_title(frame_metric + "\n" + fun_metric + "\n" + overheads)
    else:
        set_total_exec_title(frame_metric)

    # print "Total execution time:", s
    return data

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Script to generate graphs for an Ohua execution trace.')
    parser.add_argument('-f', nargs=1, help='Reference to the trace file (json).')

    args = parser.parse_args()
    plot(args.f[0], prepare_data, "accum-stats-overview-" + path.basename(args.f[0]).split(".")[0])
